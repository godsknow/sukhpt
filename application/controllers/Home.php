<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        //$this->load->model(array('mol_product'));
        $this->load->model('mol_product');
        $this->load->library('cart');
        //
    }

    public function index()
    {
        $arrContent['data'] = $this->mol_product->GetProduct();
        $this->load->view('header');
        $this->load->view('index', $arrContent);
        $this->load->view('footer');

        var_dump($arrContent['data']);
        //exit();

    }

    public function Route()
    {
        $this->load->view('header');

        if ($_GET['page'] == 'single') {
            $this->load->view('single');

        } elseif ($_GET['page'] == 'service') {
            $this->load->view('service');
        }

        $this->load->view('footer');
    }

    public function Addcart()
    {
        var_dump($_POST['pid']);

        $data['id'] = $this->input->post('pid');
        $data['name'] = $this->input->post('pname');
        $data['price'] = $this->input->post('pprice');
        $data['qty'] = 1;

        var_dump($data['name']);

        /* $data = array(
        'id' => $this->input->post('pid'),
        'name' => $this->input->post('pname'),
        'price' => $this->input->post('pprice'),
        'qty' => 1,
        );*/
        // var_dump($data);
        //$this->cart->insert($data);

        // echo $this->show_cart();
    }

    public function CartPage()
    {
        $this->load->view('header');
        $this->load->view('cartPage');
        $this->load->view('footer');
    }

    public function show_cart()
    {
        $output = '';
        $no = 0;
        foreach ($this->cart->contents() as $items) {
            $no++;
            $output .= '
		<tr>
		<td>' . $items['name'] . '</td>
		<td>' . number_format($items['price']) . '</td>
		<td>' . $items['qty'] . '</td>
		<td>' . number_format($items['subtotal']) . '</td>
		<td><button type="button" id="' . $items['rowid'] . '" class="romove_cart btn btn-danger btn-sm">Cancel</button></td>
		</tr>
		';
        }
        $output .= '
		<tr>
		<th colspan="3">Total</th>
		<th colspan="2">' . 'Rp ' . number_format($this->cart->total()) . '</th>
		</tr>
		';
        return $output;
    }
    public function load_cart()
    {
        echo $this->show_cart();
    }
    public function delete_cart()
    {
        $data = array(
            'rowid' => $this->input->post('row_id'),
            'qty' => 0,
        );
        $this->cart->update($data);
        echo $this->show_cart();
    }

}
