<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mol_product extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        //$this->load->database( 'happystation', FALSE, TRUE );

    }

    //Select All data
    public function GetProduct()
    {
        $this->db->select('id,label,name,name_eng,price');
        $this->db->from('products');
        $query = $this->db->get();
        return $query->result();

    }

}
